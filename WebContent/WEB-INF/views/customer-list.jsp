<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Customer List</title>
<link type="text/css" rel="stylesheet"
	href="<c:url value="/resources/css/style.css" />">
</head>
<body>
	<h1>Customer List</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<div id="customList">
		<h2>Ma liste de clients</h2>
		<jsp:include page="_search-customer.jsp"></jsp:include>
		<table>
			<thead>
				<tr>
					<th>id</th>
					<th>nom</th>
					<th>pr�nom</th>
					<th>email</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listCustomers}" var="item">
					<tr>
						<td>${item.id}</td>
						<td>${item.last_name}</td>
						<td>${item.first_name}</td>
						<td>${item.email}</td>
						<td><c:url var="urlCustomerUpdate" value="/customer-update">
								<c:param name="customerId" value="${item.id}" />
							</c:url> <c:url var="urlCustomerDelete" value="/customer-delete">
								<c:param name="customerId" value="${item.id}" />
							</c:url>
							<a href="${urlCustomerUpdate}" title="Mettre � jour le client" class="button">update</a>
							<a href="${urlCustomerDelete}" title="Supprimer ce client" class="button delete">delete</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>

