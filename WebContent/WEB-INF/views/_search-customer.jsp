<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<div id="search" class="divMargin">
	<form:form action="customer-search" method="GET" modelAttribute="formSearchCustomer">
		<form:input path="last_name" type="search" placeholder="rechercher un client"/>
		<form:button class="button">rechercher</form:button>
	</form:form>
</div>