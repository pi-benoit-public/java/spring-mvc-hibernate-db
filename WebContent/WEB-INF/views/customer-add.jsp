<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
	<title>Customer +</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css" />" />
</head>
<body>
	<h1>Customer +</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<div id="customerAdd">
		<h2>Ajouter un client</h2>
		<form:form action="customer-save" method="POST" modelAttribute="formAddCustomer">
			<fieldset>
				<legend>Remplir ce formulaire pour ajouter un client</legend>
				<form:input path="id" hidden="hidden"/>
				<div>
					<label for="last_name">Nom</label>
					<form:input path="last_name"/>
				</div>
				<div>
					<label for="first_name">Pr�nom</label>
					<form:input path="first_name"/>
				</div>
				<div>
					<label for="email">Email</label>
					<form:input path="email"/>
				</div>
				<input type="submit" id="submit" name="submit" class="button" value="ajouter">
			</fieldset>
		</form:form>
	</div>
</body>
</html>
