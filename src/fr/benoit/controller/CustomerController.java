package fr.benoit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.benoit.db.entity.Customer;
import fr.benoit.service.CustomerService;

@Controller
@RequestMapping("/")
public class CustomerController {

	@Autowired // Fait l'injection de d�pendance.
	private CustomerService cs;

	@RequestMapping("/customer")
	public String showPage() {
		return "customer";
	}

	@RequestMapping("/customer-list")
	public String getListCustomers(@ModelAttribute("formSearchCustomer") Customer customer, Model model) {
		List<Customer> listCustomers = cs.getCustomers();
		/*
		 * listCustomers c'est l'ID.
		 * ! Ne pas mettre de `-` sinon �a ne fonctionne pas. 
		 */
		model.addAttribute("listCustomers", listCustomers);
		// Test
//		System.out.println("coucou "+listCustomers);
		return "customer-list";
	}

	@RequestMapping("/customer-add") 
	public String showFormAddCustomer(Model model) {
		Customer c = new Customer();
		model.addAttribute("formAddCustomer", c);
		return "customer-add";
	}

	@RequestMapping("/customer-save") 
	public String saveCustomer(@ModelAttribute("formAddCustomer") Customer customer) {
		if (customer.getId() == 0)
			cs.saveCustomer(customer);
		else
			cs.updateCustomer(customer);
		return "redirect:customer-list";
	}

	@RequestMapping("/customer-update")
	public String showFormUpdateCustomer(@RequestParam("customerId") int id, Model model) {
		Customer c = cs.getCustomer(id);
		model.addAttribute("formAddCustomer", c);
		return "customer-add";
	}

	@RequestMapping("/customer-delete")
	public String deleteCustomer(@RequestParam("customerId") int id, Model model) {
		Customer c = cs.getCustomer(id);
		cs.deleteCustomer(c);
		return "redirect:customer-list";
	}

	@RequestMapping("/customer-search")
	public String searchCustomer(@ModelAttribute("formSearchCustomer") Customer customer, @RequestParam("last_name") String search, Model model) {
		List<Customer> listCustomers = cs.searchCustomer(search);
		model.addAttribute("listCustomers", listCustomers);
		return "customer-list";
	}

}
