package fr.benoit.dao;

import java.util.List;

import fr.benoit.db.entity.Customer;

public interface CustomerDAO {

	// Create
	public void saveCustomer(Customer customer);

	// Read.
	public Customer getCustomer(int id);
	public List<Customer> getCustomers();

	// Update
	public void updateCustomer(Customer customer);

	// Delete
	public void deleteCustomer(Customer customer);

	// Search
	public List<Customer> searchCustomer(String name);

}
