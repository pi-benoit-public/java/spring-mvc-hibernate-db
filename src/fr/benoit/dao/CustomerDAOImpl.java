package fr.benoit.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.benoit.db.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	// Need to inject the session factory.
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Customer getCustomer(int id) {
		return sessionFactory.getCurrentSession().get(Customer.class, id);
	}

	@Override
	public List<Customer> getCustomers() {

		// Get the current hibernate session.
		Session currentSession = sessionFactory.getCurrentSession();

		/*
		 * Create a query ... sort by last name. from Customer : c'est la classe
		 * Customer.
		 */
		Query<Customer> q = currentSession.createQuery("from Customer order by last_name", Customer.class);

		// Execute query and get result list.
		List<Customer> customers = q.getResultList();

		// Test
		// System.out.println("ici "+ customers.toString());

		return customers;

	}

	@Override
	public void saveCustomer(Customer customer) {

		// Get the current hibernate session.
		Session currentSession = sessionFactory.getCurrentSession();

		// Create a new customer.
		currentSession.save(customer);

	}

	@Override
	public void updateCustomer(Customer customer) {
		sessionFactory.getCurrentSession().update(customer);
	}

	@Override
	public void deleteCustomer(Customer customer) {
		sessionFactory.getCurrentSession().delete(customer);
	}

	@Override
	public List<Customer> searchCustomer(String name) {
		Session currentSession = sessionFactory.getCurrentSession();

		Query<Customer> q = currentSession.createQuery("FROM Customer WHERE last_name LIKE :search OR first_name LIKE :search", Customer.class)
				.setParameter("search", "%" + name + "%");

		List<Customer> customers = q.getResultList();

		return customers;
	}

}
