package fr.benoit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.benoit.dao.CustomerDAO;
import fr.benoit.db.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO cdi;

	@Override
	@Transactional
	public void saveCustomer(Customer customer) {
		cdi.saveCustomer(customer);
	}

	@Override
	@Transactional
	public Customer getCustomer(int id) {
		return cdi.getCustomer(id);
	}

	@Override
	@Transactional
	public List<Customer> getCustomers() {
		return cdi.getCustomers();
	}

	@Override
	@Transactional
	public void updateCustomer(Customer customer) {
		cdi.updateCustomer(customer);
	}

	@Override
	@Transactional
	public void deleteCustomer(Customer customer) {
		cdi.deleteCustomer(customer);
	}

	@Override
	@Transactional
	public List<Customer> searchCustomer(String name) {
		return cdi.searchCustomer(name);
	}

}
