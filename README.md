# Spring MVC avec Hibernate et connection database MySql

## Ce que l'on veut

1 - Lister les customers

2 - Ajouter un nouveau `Controller`

3 - Mettre à jour

4 - Supprimer


## Etapes de créations de Spring MVC avec hibernate et connection db

1 - Créer Customer.java

2 - Créer interface CustomerDAO.java (on veut récupérer la liste des customers). Mise en place d'une méthode.

3 - Créer CustomerDAOImpl.java qui implémente CustomerDAO.java (voir commentaires dans code).

4 - Créer CustomerController.java où on appelle la méthode créé dans 3 (voir commentaires également dans code).

5 - Vue html/css
